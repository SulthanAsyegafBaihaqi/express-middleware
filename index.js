const express = require("express");
const app = express();

// Ambil port dari environment variable
// Dengan nilai default 8000
const PORT = process.env.PORT || 8000;

app.use(express.urlencoded());

//middleware
function jalanAja(req, res, next) {
  console.log("jalan aja gih");
  //method next untuk melanjutkan ke function berikutnya
  next();
}

//middleware
function isAdmin(req, res, next) {
  if (req.query.iam === "admin") {
    next();
    return;
  }

  res.status(401).send("Kamu bukan admin");
}

function getBooks(req, res, next) {
  console.log("masuk gak yah ?");
  console.log(req.query);
  res.status(200).send(`Sukses`);
}

// GET /api/v1/books?author=
app.get("/api/v1/books", jalanAja, isAdmin, getBooks);

// POST /api/v1/books
app.post("/api/v1/books", isAdmin, (req, res) => {
  if (req.query.iam === "admin") {
    res.status(401).send("Kamu bukan admin");
  }

  console.log(req.body);
  res.status(201).send("Terima kasih sudah menambahkan buku di dalam database kami");
});

// PUT /api/v1/books/:id
app.put("/api/v1/books/:id", isAdmin, (req, res) => {
  if (req.query.iam === "admin") {
    res.status(401).send("Kamu bukan admin");
  }

  console.log(req.body);
  res.status(200).send("Sudah diupdate!");
});

app.listen(PORT, () => {
  console.log(`Express nyala di http://localhost:${PORT}`);
});
